# Makefile for Swiss Army Hammer

Compiler   := g++
Kernel     := $(shell uname -s)
Distro     := $(shell uname -r)
Target     := hexlon
TestTarget := hexlon_test
SourceDirs := $(shell ls *.cpp)
Objects := $(patsubst %.cpp, %.o, $(SourceDirs))
BinPath    := .
Libraries  :=
Includes   :=
Flags      := -g -Wall -O0

# Mac OS X specific settings
ifeq ($(Kernel), Darwin)
Compiler := clang++
endif

# Linux specific settings
ifeq ($(Kernel), Linux)
ifeq ($(Distro), 3.1.1-1-ARCH)
Compiler  := clang++
endif
endif

# target specific settings
test: Libraries += -lboost_unit_test_framework-mt
test: Sources   += tst/*.cpp

all: $(Objects)
	$(warning Building...)
	$(Compiler) $(Libraries) $(Objects) $(Sources) $(Includes) -o $(BinPath)/$(Target) $(LinkDirs) $(Flags)

.cpp.o:
	$(Compiler) $(Flags) $(Includes) -c -o $*.o $<

.cxx.o:
	$(Compiler) $(Flags) $(Includes) -c -o $*.o $<

test:
	$(warning Building Test Suite...)
	@$(Compiler) $(Libraries) $(Sources) $(Includes) -o $(BinPath)/$(TestTarget) $(LinkDirs) $(Flags)
	@-$(BinPath)/$(TestTarget) --log-level=all
	@rm $(BinPath)/$(TestTarget)
	
clean:
	rm -f $(Objects)

#multilib handling
ifeq ($(HOSTTYPE), x86_64)
LIBSELECT=64
endif
