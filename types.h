#ifndef TYPES_H_
#define TYPES_H_

//#include <boost/foreach.hpp>

//#define foreach BOOST_FOREACH

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long u64;

typedef signed char s8;
typedef signed short s16;
typedef signed int s32;
typedef signed long s64;

typedef float f32;
typedef double f64;

typedef char c8;

#endif
