#ifndef HEXLON_H
#define HEXLON_H

std::ostream& red    ( std::ostream& output );
std::ostream& green  ( std::ostream& output );
std::ostream& yellow ( std::ostream& output );
std::ostream& blue   ( std::ostream& output );
std::ostream& magenta( std::ostream& output );
std::ostream& cyan   ( std::ostream& output );
std::ostream& white  ( std::ostream& output );
std::ostream& normal ( std::ostream& output );

#endif
