#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>
#include <ostream>

#include "types.h"
#include "hexlon.h"

using namespace std;

int main(int argc, char* argv[]) {
    if (argc < 2) {
        cout << "Usage: " << argv[0] << " [filename]" << endl;
    }

    ifstream file(argv[1], ios::in | ios::binary);
    if (!file) {
        cout << "Could not read file " << argv[1] << ": file did not exist." << endl;
        file.close();
        return -1;
    }

    cout << "Type 'h' for help." << endl;

    // TODO: add terminfo + ncurses stuff to improve output formatting on linux/mac

    const int line_size = 20;
    u8 c[line_size];

    // TODO: verify/fix the bug where the last few bytes of the file might not be read or might cause a crash

    while(!file.eof()) {
        u32 address = file.tellg();
        cout << blue << setw(10) << left << dec << address << red;
        file.read((char*) &c, sizeof(u8) * line_size);

        for (int i = 0; i < line_size; i++) {
            cout << setw(5) << left;
            if (c[i] == '\n') {
                cout << "\\n";
            } else if (c[i] == '\t') {
                cout << "\\t";
            } else if (c[i] == ' ') {
                cout << "' '";
            } else if (c[i] > 32 && c[i] < 127){
                cout << c[i];
            } else {
                cout << " ";
            }
        }
        cout << endl;

        cout << blue << "0x" << setw(8) << left << hex << address << normal;

        for (int i = 0; i < line_size; i++) {
            cout << "0x" << hex << setw(3) << left << ((int) c[i]);
        }
        cout << endl;

        char in = cin.get();
        if (in == 'h') {
            cout << "s - Skip to addresss" << endl;
            cout << "q - Quit" << endl;
        } else if (in == 's') {
            u32 address;
            cout << "What address to skip to? ";
            cin >> address;
            file.seekg(address);
            cout << endl;
        } else if (in == 'q') {
            break;
        }
    }
    file.close();

    return 0;
}

#define colors

#ifdef colors
std::ostream& red    ( std::ostream& output ) { return output << "\033[0;31m"; }
std::ostream& green  ( std::ostream& output ) { return output << "\033[0;32m"; }
std::ostream& yellow ( std::ostream& output ) { return output << "\033[0;33m"; }
std::ostream& blue   ( std::ostream& output ) { return output << "\033[0;34m"; }
std::ostream& magenta( std::ostream& output ) { return output << "\033[0;35m"; }
std::ostream& cyan   ( std::ostream& output ) { return output << "\033[0;36m"; }
std::ostream& white  ( std::ostream& output ) { return output << "\033[0;37m"; }
std::ostream& normal ( std::ostream& output ) { return output << "\033[0m"; }
#endif

#ifndef colors
std::ostream& red    ( std::ostream& output ) { return output; }
std::ostream& green  ( std::ostream& output ) { return output; }
std::ostream& yellow ( std::ostream& output ) { return output; }
std::ostream& blue   ( std::ostream& output ) { return output; }
std::ostream& magenta( std::ostream& output ) { return output; }
std::ostream& cyan   ( std::ostream& output ) { return output; }
std::ostream& white  ( std::ostream& output ) { return output; }
std::ostream& normal ( std::ostream& output ) { return output; }
#endif

